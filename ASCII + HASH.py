import random
import hashlib

name = input('Enter Your Name :')

NAME_ASCII = ''
print(NAME_ASCII)

for i in range(len(name)):
    print( str(ord(name[i]))  +  " -> This is ASCII code For " + str(name[i]))
    NAME_ASCII += str( ord(name[i]))

print(NAME_ASCII)

hsh_rand = random.randrange(3)

NAME_ASSH = ''

if(hsh_rand == 0):
    NAME_ASSH =  hashlib.sha224(NAME_ASCII.encode()).hexdigest()

elif(hsh_rand == 1):
    NAME_ASSH =  hashlib.sha256(NAME_ASCII.encode()).hexdigest()

elif(hsh_rand == 2):
    NAME_ASSH =  hashlib.sha384(NAME_ASCII.encode()).hexdigest()

print("You Name ASCII + HASH = " + NAME_ASSH )